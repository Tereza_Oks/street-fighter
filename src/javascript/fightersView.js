import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);
        console.log('clicked')
        console.log(fighter);

        const fighterDetails = await fighterService.getFighterDetails(fighter._id);

        this.createFighterPreview(fighterDetails);


        // get from map or load info and add to fightersMap
        // show modal with fighter info
        // allow to edit health and power in this modal
    }

    createFighterPreview(fighterDetails) {
        const fighterInfo = document.createElement("div");
        fighterInfo.innerHTML = `
        <p>Name: ${fighterDetails.name}</p>
        <p>Health: ${fighterDetails.health}</p>
        <p>Attack: ${fighterDetails.attack}</p>
        <p>defense: ${fighterDetails.defense}</p>
        `;
        fighterInfo.style.position = "absolute";
        fighterInfo.style.marginLeft = "50%";
        fighterInfo.style.marginRight = "50%";
        this.element.append(fighterInfo);
    }
}

export default FightersView;